import java.util.Scanner;

public class PrimzahlenTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		boolean prim = false;
		int a = 5;
		long zeit[]= new long[a];
		while (true) {
			long zahl = (long) ((Math.random() * 10000000) + 1);
			for (int i = 0;i<a;i++) {
				Uhr.start();
				prim = Primzahl.istKeinePrimzahl(zahl);
				zeit[i] = Uhr.stop();
				System.out.println(i+1+" Zeit: "+zeit[i]);
			}
			System.out.println("Zahl: " + zahl);
			System.out.println("Primzahl: " + prim);
			System.out.println("Durchschnitt: " + durch(zeit));
			s.next();											//"pausiert" den Algorythmus
		}
	}
	public static double durch(long[] zahlen) {
		long a =0;
		for(int i = 0;i<zahlen.length;i++) {
			a += zahlen[i];
		}
		double b =((a*100)/(zahlen.length));
		return b/100;
	}

}
