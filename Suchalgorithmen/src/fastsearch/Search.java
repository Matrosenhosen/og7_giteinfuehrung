package fastsearch;

public class Search {
	public static int suche(int start, int ende, int wert, int[] array) {
		if (start <= ende) {
			int n = ((start + ende) / 2);
			int m = (start + (ende - start) * ((wert - array[start]) / (array[ende] - array[start])));
			if (n > m) {
				int nm = n;
				n = m;
				m = nm;
			}
			if (wert == array[n])
				return n;
			else if (wert == array[m])
				return m;
			else if (wert < array[n])
				return suche(start, n - 1, wert, array);
			else if (wert < array[m])
				return suche(n + 1, m - 1, wert, array);
			else
				return suche(m + 1, ende, wert, array);
		}
		return -1;
	}
}
