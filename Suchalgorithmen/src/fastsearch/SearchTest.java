package fastsearch;

import java.util.Scanner;

public class SearchTest {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Listengröße in Millionen: ");
		int[] list = bekommeSortierteListe(s.nextInt() * 1000000);
		while (true) {
			System.out.print("Gesuchte Zahl: ");
			int gesuchteZahl = s.nextInt();
			System.out.println("Suche: ");
			char a = s.next().toLowerCase().charAt(0);
			if (a == 'l') {
				for (int i = 0; i < 5; i++) {
					Uhr.start();
					lineareSuche(list, gesuchteZahl);
					System.out.println(Uhr.stop());
				}
			} else if (a == 'f') {
				for (int i = 0; i < 5; i++) {
					Uhr.start();
					System.out.println("Index: " + Search.suche(0, list.length - 1, gesuchteZahl, list));
					System.out.println("Zeit: " + Uhr.stop());
				}
			} else if (a == 'q') {
				for (int i = 0; i < 5; i++) {
					Uhr.start();
					System.out.println("Index: " + binaereSuche(list, gesuchteZahl));
					System.out.println("Zeit: " + Uhr.stop());
				}
			}

		}
	}

	public static int[] bekommeSortierteListe(int elemente) {
		int list[] = new int[elemente];
		for (int i = 0; i < elemente; i++) {
			list[i] = i;
		}
		return list;
	}

	public static int lineareSuche(int[] list, int gesuchteZahl) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == gesuchteZahl)
				return i;
		}
		return -1;
	}

	public static int binaereSuche(int[] m, int gesuchteZahl) {
		int links = 0;
		int rechts = m.length - 1;

		while (links <= rechts) {
			int mitte = links + ((rechts - links) / 2);

			if (m[mitte] == gesuchteZahl)
				return mitte;
			else if (m[mitte] > gesuchteZahl)
				rechts = mitte - 1;
			else
				links = mitte + 1;
		}

		return -1;
	}
}
