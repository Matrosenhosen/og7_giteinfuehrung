package keyStore;

import java.util.Arrays;

public class KeyStore01 {
	private String[] schluessel;
	private int zaehler;

	public KeyStore01() {
		schluessel = new String[100];
	}

	public KeyStore01(int length) {
		schluessel = new String[length];
	}

	public boolean add(String e) {
		if (zaehler < schluessel.length) {
			schluessel[zaehler] = e;
			zaehler++;
			return true;
		} else
			return false;
	}

	public void clear() {
		schluessel = new String[schluessel.length];
		zaehler = 0;
	}

	public String get(int index) {
		return schluessel[index];
	}

	public int indexOf(String str) {
		for (int i = 0; i < schluessel.length; i++) {
			if (schluessel[i].equals(str))
				return i;
		}
		return -1;
	}

	public boolean remove(int index) {
		if (index > schluessel.length||index<0)
			return false;
		else {
			schluessel[index] = null;
			return true;
		}
	}

	public boolean remove(String str) {
		int index = indexOf(str);
		if (index != -1) {
			schluessel[index] = null;
			return true;
		} else
			return false;
	}

	public int size() {
		int s = 0;
		for (int i = 0; i <= zaehler; i++) {
			if (schluessel[i] != null)
				s++;
		}
		return s;
	}

	public String toString() {
		return "KeyStore01 [schluessel=" + Arrays.toString(schluessel) + "]";
	}
}
