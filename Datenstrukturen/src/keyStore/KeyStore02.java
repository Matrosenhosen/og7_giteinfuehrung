package keyStore;

import java.util.ArrayList;

public class KeyStore02 {
	private ArrayList<String> list;

	public KeyStore02() {
		list = new ArrayList<String>();
	}

	public void add(String e) {
		list.add(e);
	}

	public void clear() {
		list.clear();
	}

	public String get(int index) {
		return list.get(index);
	}

	public int indexOf(String str) {
		return list.indexOf(str);
	}

	public boolean remove(int index) {
		if (index < list.size()) {
			list.remove(index);
			list.trimToSize();
			return true;
		}return false;
	}

	public boolean remove(String str) {
		int index = list.indexOf(str);
		if (index != -1) {
			list.remove(index);
			list.trimToSize();
			return true;
		} else
			return false;
	}

	public int size() {
		return list.size();
	}

	public String toString() {
		return "KeyStore02 [schluessel=" + list.toString() + "]";
	}
}
