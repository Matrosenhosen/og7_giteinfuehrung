package buch;

import java.util.*;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {

		List<Buch> buchliste = new LinkedList<Buch>();
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				buchliste.add(erstelleBuch());
				break;
			case '2':
				System.out.print("ISBN: ");
				Buch i = findeBuch(buchliste, myScanner.next());
				if (i != null) {
					System.out.println(i.toString());
				}
				System.out.println("Buch nicht gefunden");
				break;
			case '3':
				if (loescheBuch(buchliste))
					System.out.println("Buch wurde entfernt!");
				else
					System.out.println("Buch konnte nicht gefunden werden!");
				break;
			case '4':
				System.out.println("Gr��te ISBN: " + ermitteleGroessteISBN(buchliste));
				break;
			case '5':
				buchliste.forEach(b -> System.out.println(b.toString()));
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		isbn = "ISBN-" + isbn;
		for (int i = 0; i < buchliste.size(); i++)
			if (buchliste.get(i).getIsbn().equals(isbn))
				return buchliste.get(i);
		return null;
	}

	public static Buch erstelleBuch() {
		Scanner s = new Scanner(System.in);
		System.out.println("Autor: ");
		String autor = s.next();
		System.out.println("ISBN: ");
		String isbn = "ISBN-" + s.next();
		System.out.println("Titel: ");
		String titel = s.next();
		return new Buch(autor, titel, isbn);
	}

	public static boolean loescheBuch(List<Buch> list) {
		System.out.print("ISBN: ");
		Scanner s = new Scanner(System.in);
		String isbn = "ISBN-" + s.next();
		for (int i = 0; i < list.size(); i++)
			if (list.get(i).getIsbn().equals(isbn)) {
				list.remove(i);
				return true;
			}
		return false;
	}

	public static String ermitteleGroessteISBN(List<Buch> list) {
		Buch g = list.get(0);
		for (int i = 1; i < list.size(); i++) {
			if (g.compareTo(list.get(i)) < 0) {
				g = list.get(i);
			}
		}
		return g.getIsbn();
	}
}