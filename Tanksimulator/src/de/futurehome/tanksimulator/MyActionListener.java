package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand < 96) {
				fuellstand = fuellstand + 5;
			}else {
				fuellstand = 100.0;
			}
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText(fuellstand + "l");
			f.lblProzent.setText(((fuellstand/100)*100)+"%");
		}
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand > 1) {
				fuellstand -= 2;
			} else {
				fuellstand = 0;
			}
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText(fuellstand + "l");
			f.lblProzent.setText(((fuellstand/100)*100)+"%");
		}
		if (obj == f.btnZurueck) {
			f.myTank.setFuellstand(0);
			f.lblFuellstand.setText("0.0l");
			f.lblProzent.setText("0.0%");
		}

	}
}