package karte;

public class Held {
	private String name;
	private String typ;
	private String beschreibung;
	private int maxHp;
	private int aktHp;
	private int angriff;
	private int ruestung;
	private int magieresistenz;
	private String bildpfad;

	public Held(String name, String typ, String beschreibung, int maxHp, int ruestung, int magieresistenz,
			int angriff) {
		super();
		this.setName(name);
		this.setTyp(typ);
		this.setBeschreibung(beschreibung);
		this.setMaxHp(maxHp);
		this.setAktHp(maxHp);
		this.setAngriff(angriff);
		this.setRuestung(ruestung);
		this.setMagieresistenz(magieresistenz);
		this.setBildpfad(".src/bilder/" + name + ".jpeg");
	}

	public Held() {
		super();
		this.setAktHp(0);
		this.setAngriff(0);
		this.setBeschreibung("");
		this.setBildpfad(null);
		this.setMagieresistenz(0);
		this.setMaxHp(0);
		this.setName("");
		this.setRuestung(0);
		this.setTyp("");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getMaxHp() {
		return maxHp;
	}

	public void setMaxHp(int maxHp) {
		this.maxHp = maxHp;
	}

	public int getAktHp() {
		return aktHp;
	}

	public void setAktHp(int aktHp) {
		if (aktHp < 0)
			this.aktHp = 0;
		else
			this.aktHp = aktHp;
	}

	public int getAngriff() {
		return angriff;
	}

	public void setAngriff(int angriff) {
		this.angriff = angriff;
	}

	public int getRuestung() {
		return ruestung;
	}

	public void setRuestung(int ruestung) {
		this.ruestung = ruestung;
	}

	public int getMagieresistenz() {
		return magieresistenz;
	}

	public void setMagieresistenz(int magieresistenz) {
		this.magieresistenz = magieresistenz;
	}

	public String getBildpfad() {
		return bildpfad;
	}

	public void setBildpfad(String bildpfad) {
		this.bildpfad = bildpfad;
	}

	public void heilen() {
		this.setAktHp(this.getMaxHp());
	}

	public void leiden(int angriff) {
		if ((angriff - this.getRuestung()) <= 0)
			this.setAktHp(this.getAktHp() - 1);
		else
			this.setAktHp(this.getAktHp() - (angriff - this.getRuestung()));
	}

	public int angreifen() {
		return this.getAngriff();
	}

	@Override
	public String toString() {
		return "Held [name=" + name + ", typ=" + typ + ", beschreibung=" + beschreibung + ", maxHp=" + maxHp
				+ ", aktHp=" + aktHp + ", angriff=" + angriff + ", ruestung=" + ruestung + ", magieresistenz="
				+ magieresistenz + ", bildpfad=" + bildpfad + "]";
	}

}
