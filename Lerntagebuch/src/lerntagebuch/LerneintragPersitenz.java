package lerntagebuch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class LerneintragPersitenz {
//	public static void main(String[] args) {
//		read();
////		reader(new File("miriam.dat"));
//	}

	public static void read() {
		try {
			LineNumberReader lr = new LineNumberReader(new FileReader("miriam.dat"));
			BufferedReader br = new BufferedReader(lr);
			String s = br.readLine();
			s = s.substring(s.indexOf("M"), s.length());
			while (s != null) {
				System.out.println(s);
				s = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void reader(File file) {
		try {
			LineNumberReader lr = new LineNumberReader(new FileReader(file));
			BufferedReader br = new BufferedReader(lr);
			String s = br.readLine();
			s = s.substring(s.indexOf("M"), s.length());
			while (s != null) {
				System.out.println(s);
				s = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
