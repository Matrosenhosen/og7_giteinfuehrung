package tornado;

public class Trainer extends Person {
	private char lizensklasse;
	private double gehalt;

	public Trainer() {
		super();
	}

	public Trainer(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			char lizensklasse, double gehalt) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt);
		this.gehalt = gehalt;
		this.lizensklasse = lizensklasse;
	}

	public char getLizensklasse() {
		return lizensklasse;
	}

	public void setLizensklasse(char lizensklasse) {
		this.lizensklasse = lizensklasse;
	}

	public double getGehalt() {
		return gehalt;
	}

	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}

	public String toString() {
		return "Name: " + this.vorname + " " + this.nachname + " Telefonnummer: " + this.telefonnummer
				+ " Jahresbeitrag Bezahlt: " + this.jahresbeitragBezahlt + " Lizensklasse: " + this.lizensklasse
				+ " Gehalt: " + this.gehalt + "�";
	}
}
