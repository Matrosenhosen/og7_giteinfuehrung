package tornado;

public class Mannschaftsleiter extends Spieler {

	private String mannschaftsname;
	private int rabatt;

	public Mannschaftsleiter() {
		// TODO Auto-generated constructor stub
	}

	public Mannschaftsleiter(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			int trikotnummer, String spielposition, String mannschaftsname, int rabatt) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt, trikotnummer, spielposition);
		this.mannschaftsname = mannschaftsname;
		this.rabatt = rabatt;
	}

	public String getMannschaftsleiter() {
		return mannschaftsname;
	}

	public void setMannschaftsleiter(String mannschaftsleiter) {
		this.mannschaftsname = mannschaftsleiter;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}

	public String toString() {
		return "Name: " + this.vorname + " " + this.nachname + " Telefonnummer: " + this.telefonnummer
				+ " Jahresbeitrag Bezahlt: " + this.jahresbeitragBezahlt + " Trikotnummer: " + this.trikotnummer
				+ " Spielposition: " + this.spielposition + " Mannschaftsname: " + this.mannschaftsname + " Rabatt: "
				+ this.rabatt;
	}
}
