package tornado;

public abstract class Person {
	protected String nachname;
	protected String vorname;
	protected String telefonnummer;
	protected boolean jahresbeitragBezahlt;

	public Person() {

	}

	public Person(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt) {
		this.nachname = nachname;
		this.vorname = vorname;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isJahresbeitragBezahlt() {
		return jahresbeitragBezahlt;
	}

	public void setJahresbeitragBezahlt(boolean jahresbeitragBezahlt) {
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}

	public abstract String toString();

}
