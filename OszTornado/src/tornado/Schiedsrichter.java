package tornado;

public class Schiedsrichter extends Person {

	private int gepfiffeneSpiele;

	public Schiedsrichter() {
	}

	public Schiedsrichter(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			int gepfiffeneSpiele) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public String toString() {
		return "Name: " + this.vorname + " " + this.nachname + " Telefonnummer: " + this.telefonnummer
				+ " Jahresbeitrag Bezahlt: " + this.jahresbeitragBezahlt + " Gepfiffene Spiele: "
				+ this.gepfiffeneSpiele;
	}

}
