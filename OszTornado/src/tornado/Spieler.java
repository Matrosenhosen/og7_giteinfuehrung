package tornado;

public class Spieler extends Person {
	protected int trikotnummer;
	protected String spielposition;

	public Spieler() {
		super();
	}

	public Spieler(String nachname, String vorname, String telefonnummer, boolean jahresbeitragBezahlt,
			int trikotnummer, String spielposition) {
		super(nachname, vorname, telefonnummer, jahresbeitragBezahlt);
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public String toString() {
		return "Name: " + this.vorname + " " + this.nachname + " Telefonnummer: " + this.telefonnummer
				+ " Jahresbeitrag Bezahlt: " + this.jahresbeitragBezahlt + " Trikotnummer: " + this.trikotnummer
				+ " Spielposition: " + this.spielposition;
	}

}