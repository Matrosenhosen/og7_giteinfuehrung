package array;
import java.util.Arrays;
/**
  *
  * Übungsklasse Felder
  *
  * @version 1.0 vom 21.09.2018
  * @author Dominik Conrad
  */

public class Felder {

  //unsere Zahlenliste zum Ausprobieren
  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
  
  //Konstruktor
  public Felder(){}

  //Methode die Sie implementieren sollen
  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
  
  //die Methode soll die größte Zahl der Liste zur�ckgeben
  public int maxElement(){
	  int r=zahlenliste[0];
    for(int i = 1;i<zahlenliste.length;i++) {
    	if(zahlenliste[i]>r) {
    		r=zahlenliste[i];
    	}
    }
    return r;
  }

  //die Methode soll die kleinste Zahl der Liste zur�ckgeben
  public int minElement(){
	  int r = zahlenliste[0];
	  for(int i = 1;i<zahlenliste.length;i++) {
		  if(r>zahlenliste[i]) {
			  r=zahlenliste[i];
		  }
	  }
    return r;
  }
  
  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
  public double durchschnitt(){
	  double r = zahlenliste[0];
	  for(int i=1;i<zahlenliste.length;i++) {
		  r+=zahlenliste[i];
	  }r= r/zahlenliste.length;
    return r;
  }

  //die Methode soll die Anzahl der Elemente zurückgeben
  public int anzahlElemente(){
    return zahlenliste.length;
  }

  //die Methode soll die Liste ausgeben
  public String toString(){
	  StringBuilder b = new StringBuilder();
		for (int i = 0; i < zahlenliste.length; i++) {
			b.append(zahlenliste[i]);
			if (i < zahlenliste.length - 1) {
				b.append(",");
			}
		}
		return b.toString();
  }

  //die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
  //dem Feld vorhanden ist
  public boolean istElement(int zahl){
	  for(int i=0;i<zahlenliste.length;i++) {
		  if(zahlenliste[i]==zahl) return true;
	  }
    return false;
  }
  
  //die Methode soll das erste Vorkommen der
  //als Parameter übergebenen  Zahl liefern oder -1 bei nicht vorhanden
  public int getErstePosition(int zahl){
	  for(int i = 0;i<zahlenliste.length;i++) {
		  if(zahl==zahlenliste[i]) {
			  return i;
		  }
	  }
    return -1;
  }
  
  //die Methode soll die Liste aufsteigend sortieren
  //googlen sie mal nach Array.sort() ;)
  public void sortiere(){
	  Arrays.sort(zahlenliste);
  }

  public static void main(String[] args) {
    Felder testenMeinerLösung = new Felder();
    System.out.println(testenMeinerLösung.maxElement());
    System.out.println(testenMeinerLösung.minElement());
    System.out.println(testenMeinerLösung.durchschnitt());
    System.out.println(testenMeinerLösung.anzahlElemente());
    System.out.println(testenMeinerLösung.toString());
    System.out.println(testenMeinerLösung.istElement(9));
    System.out.println(testenMeinerLösung.getErstePosition(5));
    testenMeinerLösung.sortiere();
    System.out.println(testenMeinerLösung.toString());
  }
}