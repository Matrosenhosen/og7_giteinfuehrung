package array;

public class ArrayHelper {
	public static String convertArrayToString(int[] a) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < a.length; i++) {
			b.append(a[i]);
			if (i < a.length - 1) {
				b.append(",");
			}
		}
		return b.toString();
	}
}
