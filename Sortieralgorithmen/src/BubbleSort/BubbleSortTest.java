package BubbleSort;
public class BubbleSortTest {
	public static void main(String[] args) {
		int[] list = new int[6];
		for(int i=0;i<6;i++) {
			list[i]=10-i;
		}
		ausgabe(list);
		list = BubbleSort.bubblesort(list);
		ausgabe(list);
		
	}
	public static void ausgabe(int[] list) {
		for(int i = 0; i<list.length-1;i++) {
			System.out.print(list[i]+",");
		}
		System.out.println(list[list.length-1]);
	}
}
