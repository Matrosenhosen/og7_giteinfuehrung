package BubbleSort;

public class BubbleSort {
	public static int[] bubblesort(int[] list) {
		int t;
		for (int i = 1; i < list.length; i++) {
			for (int j = 0; j < list.length - i; j++) {
				if (list[j] > list[j + 1]) {
					t = list[j];
					list[j] = list[j + 1];
					list[j + 1] = t;
				}

			}
		}
		return list;
	}
}
