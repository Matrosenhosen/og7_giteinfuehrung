package mergesort;

public class MergesortTest {

	public static void main(String[] args) {
		int size = 20;
		int[] list = new int[size];
		for(int i=0;i<size;i++) {
			list[i]=1;
		}
		for(int i=0;i<size;i++) {
			System.out.print(list[i]+",");
		}
		long time = System.currentTimeMillis();
		list = Mergesort.sort(list, 0, list.length-1);
		System.out.println("Done"+(System.currentTimeMillis()-time));
		for(int i=0;i<size;i++) {
			System.out.print(list[i]+",");
		}
	}

}
