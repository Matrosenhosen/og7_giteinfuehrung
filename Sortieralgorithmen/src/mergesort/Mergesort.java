package mergesort;

public class Mergesort {

	static void merge(int liste[], int l, int m, int r) {
		int n1 = m - l + 1;
		int n2 = r - m;
		int li[] = new int[n1];
		int re[] = new int[n2];

		for (int i = 0; i < n1; ++i)
			li[i] = liste[l + i];
		for (int j = 0; j < n2; ++j)
			re[j] = liste[m + 1 + j];

		int i = 0, j = 0;

		int k = l;
		while (i < n1 && j < n2) {
			if (li[i] <= re[j]) {
				liste[k] = li[i];
				i++;
			} else {
				System.out.println("Tausche: " + li[i] + " mit " + re[j]);
				liste[k] = re[j];
				j++;
			}
			k++;
		}

		while (i < n1) {
			liste[k] = li[i];
			i++;
			k++;
		}

		while (j < n2) {
			liste[k] = re[j];
			j++;
			k++;
		}
	}

	static int[] sort(int arr[], int l, int r) {
		if (l < r) {
			int m = (l + r) / 2;

			sort(arr, l, m);
			sort(arr, m + 1, r);

			merge(arr, l, m, r);
		}
		return arr;
	}
}
